<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.css">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="style.css">
    <title>Birth Stone</title>
</head>

<body>
    <div class="d-flex justify-content-center align-items-center flex-column vh-100">
        <h1 class="text-center">Your Birthday! <br>Birthstone and Zodiac</h1>
        <form action="controllers/process_birthstone.php" method="POST">
            <div class="d-flex justify-content-center align-items-center">
                <input type="text" name="name" value="" placeholder="Name" class="input-name">
            </div>
            <h2 class="text-center">Please Enter Birthdate</h2>
            <div class="d-flex justify-content-center align-items-center flex-row">
                <select name="month" id="month">
                    <option value="" name="" >Month</option>
                    <option value="January" name="">January</option>
                    <option value="February" name="">February</option>
                    <option value="March" name="">March</option>
                    <option value="April" name="">April</option>
                    <option value="May" name="">May</option>
                    <option value="June" name="">June</option>
                    <option value="July" name="">July</option>
                    <option value="August" name="">August</option>
                    <option value="September" name="">September</option>
                    <option value="October" name="">October</option>
                    <option value="November" name="">November</option>
                    <option value="December" name="">December</option>

                </select>
                <input type="number" name="day" value="" placeholder="Day" class="form-control">
                <input type="number" name="" value="" placeholder="Year" class="form-control">



            </div>

            <div class=" d-flex justify-content-center align-items-center">
            <button type="submit" name="submit" value="Submit" id=""class="btn btn-success">Submit</button>
            </div>




        </form>

    </div>
</body>

</html>